#!/usr/bin/env python

import os
import sys
import re
import datetime
from distutils import spawn
import subprocess
import tempfile
from collections import defaultdict


DEPS = ['pdftops', 'pstotext']
MONTHS = dict((m, n) for n, m in enumerate(('GENNAIO',
                                            'FEBBRAIO',
                                            'MARZO',
                                            'APRILE',
                                            'MAGGIO',
                                            'GIUGNO',
                                            'LUGLIO',
                                            'AGOSTO',
                                            'SETTEMBRE',
                                            'OTTOBRE',
                                            'NOVEMBRE',
                                            'DICEMBRE'), 1))

name_regexp = re.compile('^\w{2,}')
vote_regexp = re.compile('^[FCAMV]$')


def get_missing_deps(deps):
    return [dep for dep in deps if not spawn.find_executable(dep)]


def get_temp_file_path(suffix=None):
    """Returns the path to a previously existing temporary file.
    """
    with tempfile.NamedTemporaryFile(suffix=suffix) as fd:
        return fd.name


def invoke(command_seq):
    proc = subprocess.Popen(command_seq, stdout=subprocess.PIPE)
    return proc.communicate()


def find(ns, k, margin, default):
    assert hasattr(k, 'real')
    try:
        return ns[k]
    except KeyError:
        tol = list(xrange(-margin, margin+1))
        tol.remove(0)
        for i in tol:
            try:
                return ns[k+i]
            except KeyError:
                pass
    return default


class Page(object):
    tolerance = 5

    def __init__(self, data):
        self._parse_headers(data)
        self._parse_voters(data)
        self._data = data

    def _parse_headers(self, data):
        sequence = []
        headers = {}
        for idx, datum in enumerate(data, 0):
            if datum[0][-1] == "DEPUTATI":
                sequence.append((data[idx-1], datum[1:]))

        assert sequence
        self._sequence = sequence

        for sessions, issues in sequence:
            ses = {int(x): int(n) for x, y, w, z, n in sessions}
            iss = {int(x): int(n) for x, y, w, z, n in issues}
            headers.update(
                {x: (find(ses, x, 2, 0), i) for x, i in iss.items()}
            )

        self.headers = headers

        span = len(self.headers) // len(self._sequence)
        items = sorted(self.headers.items())

        for name, idx in (('sessions', 0), ('issues', 1)):
            setattr(self, name, [v[idx] for k, v in items][0:span])

    def _parse_voters(self, sections):
        vote_rxp = re.compile("^[FCAMVT]$")
        ignore_rxp = re.compile("[0-9]+|DEPUTATI")

        voters = {}

        for sec in sections:
            name = []
            mapping = {}
            for x, y, w, z, n in sec:
                if ignore_rxp.match(n):
                    break
                elif vote_rxp.match(n):
                    pos = int(x)
                    assert pos not in mapping
                    mapping[pos] = n
                else:
                    name.append(n)

            if name:
                voters[" ".join(name)] = mapping

        self._voters = voters

    @property
    def voters(self):
        return sorted(self._voters.keys())

    def _get_voter(self, name):
        try:
            voter = self._voters[name]
        except KeyError:
            return None

        headers = self.headers.items()
        votes = defaultdict(dict)

        for pos, pair in headers:
            sess, issu = pair
            vote = find(voter, pos, self.tolerance, None)

            if vote is not None:
                votes[sess].update({issu: vote})

        return votes

    def __getitem__(self, item):
        return self._get_voter(item)

    def get_votes(self, name):
        voter = self._get_voter(name)
        votes = []
        for s, i in zip(self.sessions, self.issues):
            sess = voter.get(s)
            votes.append(sess.get(i, None) if sess else None)

        return votes

    def pformat(self, headers=True):
        lines = []
        tab = " "
        if headers:
            lines.append(tab.join(str(e) for e in self.sessions))
            lines.append(tab.join(str(e) for e in self.issues))
            lines.append(tab.join("-" for el in self.issues))

        for name in sorted(self.voters):
            line = tab.join(el or "." for el in self.get_votes(name) + [name])
            lines.append(line)

        return "\n".join(lines)

    def pprint(self):
        print self.pformat()


class Document(object):
    def __init__(self, stream):
        self._stream = stream
        self._data = ()
        self.pages = None
        self.get_metadata()

    def get_metadata(self):
        rxp = '(\d{1,2})\n.+(%s)\n.+(\d{4})\n' % "|".join(MONTHS)
        day, m, year = re.search(rxp, self._stream).groups()
        data = {
            'date': datetime.date(int(year), MONTHS[m], int(day))
        }

        return data

    def chop(self):
        """Into pages.
        """
        seq = re.split(
            '(\n[0-9\s]+ELENCO\n[0-9\t A-Za-z\n{1}\-\.\xad]+[\s]+)',
            self._stream
        )
        seq2 = [pg for i, pg in enumerate(seq, 1) if i % 2]

        # Remove footer
        if seq2[-1].startswith('*'):
            seq2.pop()

        normalized = []
        for idx, page in enumerate(seq2, 0):
            normalized.append(
                page if page.startswith('   ') else '   ' + page
            )

        seq3 = [re.split('\n\n', page) for page in normalized]

        pages = []
        for page in seq3:
            sections = []
            for el in page:
                tmp = []
                for line in el.split('\n'):
                    x, y, w, z, name = line.split()
                    tmp.append((x, y, w, z, name))
                sections.append(tmp)
            pages.append(sections)
        self._data = pages

    def grok(self):
        self.pages = []
        for data in self._data:
            self.pages.append(Page(data))

    def process(self):
        self.chop()
        self.grok()

    def pformat(self):
        text = [self.pages[0].pformat()]
        for pg in self.pages[1:]:
            text.append(pg.pformat(headers=False))
        return "\n".join(text)

    def pprint(self):
        print self.pformat()


class Parser(object):
    def __init__(self):
        self.ps_path = None
        self.stream = None
        self.lines = ()
        self.headers = ()

    def open(self, path):
        self.source_path = path
        self.convert()
        self.extract()
        self.parse()

    def convert(self):
        src_path = self.source_path
        dst_path = get_temp_file_path(suffix='.ps')
        out, err = invoke(['pdftops', src_path, dst_path])
        if err is None and os.path.isfile(dst_path):
            self.ps_path = dst_path

    def extract(self):
        if self.ps_path:
            out, err = invoke(['pstotext', '-bboxes', self.ps_path])
            if err is None and out:
                self.stream = out

    def parse(self):
        if self.stream:
            self.doc = Document(self.stream)
            self.doc.process()

    def close(self):
        if self.ps_path:
            os.unlink(self.ps_path)

    def pprint(self):
        self.doc.pprint()


def main(pdf_path):
    parser = Parser()
    parser.open(path)
    parser.pprint()
    parser.close()


def usage(name):
    print "Usage:"
    print "\t%s <path-to-pdf-file>" % name


if __name__ == '__main__':
    missing = get_missing_deps(DEPS)
    if missing:
        for dep in missing:
            sys.stderr.write("'%s' not installed.\n" % dep)
        sys.stderr.write("Aborting...\n")
        sys.exit(1)
    else:
        if len(sys.argv) == 2:
            path = sys.argv[1]
            main(path)
        else:
            usage(sys.argv[0])
            sys.exit(1)
